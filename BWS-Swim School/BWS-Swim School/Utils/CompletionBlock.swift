//
//  CompletionBlock.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 25/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation

class CompletionBlock<Result> {
    func make(onResult: @escaping (Result) -> Void, onError: @escaping (Error) -> Void) -> ((Result?, Error?) -> Void) {
        return { (maybeResult, maybeError) in
            if let result = maybeResult {
                onResult(result)
            } else if let error = maybeError {
                onError(error)
            } else {
                onError(Errors.DefaultError)
            }
        }
    }
}
enum Errors: Error {
    case DefaultError
}
