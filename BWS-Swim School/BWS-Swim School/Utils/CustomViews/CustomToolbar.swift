//
//  CustomToolbar.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 18/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

class CustomToolbar: UIStackView {
    
    convenience init(allSubViews: [UIView]) {
        self.init()
        self.addArrangedSubviews(allSubViews)
        self.distribution = .fill
        self.addBackground(color: Theme.Color.white70)
        self.axis = .horizontal
    }
    
    static func createBasicDoneButtonToolbar(completionHandler: @escaping ()-> Void) -> CustomToolbar {
            let spacerView = UIView()
            let doneBtn = UIButton()
            doneBtn.setTitle("Done", for: UIControl.State.normal)
            doneBtn.setTitleColor(UIColor.blue, for: UIControl.State.normal)
            
            doneBtn.onTap { gesture in
                completionHandler()
            }
            
            let toolBar = CustomToolbar(allSubViews: [spacerView, doneBtn])
            NSLayoutConstraint.activate([
                doneBtn.widthAnchor.constraint(equalToConstant: 80)
            ])
            return toolBar
    }
}
