//
//  CompactSizePortraitOnlyViewController.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 30/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import  UIKit

class CompactSizePortraitOnlyViewController: UIViewController {
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        //Code below not quite logical: the supported interface orientation is not called on current size class unfortunately. So an "OR" statement is used- if either the initial loaded size class is compact in any direction, the orientation is forced portrait.
        get {
            if view.traitCollection.verticalSizeClass == .compact || view.traitCollection.horizontalSizeClass == .compact {
                return .portrait
            }
            else {
                return .all
            }
        }
    }
}
