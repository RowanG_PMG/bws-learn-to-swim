//
//  DateBirthField.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 7/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit
import  RxSwift
import RxCocoa

class FloatPickerField : UIView {
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate var placeholderVal: String = ""
    
    var errorVal: BehaviorRelay<String> = BehaviorRelay(value: "")
    var data: BehaviorRelay<String?> = BehaviorRelay(value: "")
    
    fileprivate let floatingLabel = UILabel()
    fileprivate let label = UILabel()
    fileprivate let downArrow = UIImageView()
    fileprivate let underline = UIView()
    
    fileprivate let floatingLabelHeight = 16
    fileprivate let gapBetweenDateAndFloatText = 15
    fileprivate var floatingLabelHeightConstraint: NSLayoutConstraint!
    fileprivate var gapBetweenFloatAndLabelConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        observeDataToUpdateUI()
    }
    
    convenience init (placeholder: String) {
        self.init()
        self.placeholderVal = placeholder
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setErrorState(error: String) {
        errorVal.accept(error)
    }
    
    fileprivate func observeDataToUpdateUI() {
        
        let dataObservable =  data.asObservable()
        let errorObservable = errorVal.asObservable()
        
        Observable.combineLatest(dataObservable, errorObservable).subscribe(onNext: {(text, error) in
            guard let value = text else {
                self.clearValue()
                return
            }
            
            if value != "" {
                self.label.text = value
                self.label.textColor = UIColor.white
                self.showFloatingLabel()
            } else {
                self.clearValue()
            }
            
            if (error != "") {
                self.showFloatingLabel()
                self.floatingLabel.text = error
                self.floatingLabel.textColor = UIColor.red
                self.underline.backgroundColor = UIColor.red
            } else {
                self.floatingLabel.textColor = Theme.Color.plainBlue
                self.floatingLabel.text = self.placeholderVal
                self.underline.backgroundColor = Theme.Color.darkerBlue
            }
        }
        )
    }
    
    fileprivate func setupUI() {
        floatingLabel.font = Theme.Font.make(13, weight: Theme.FontWeight.medium)
        floatingLabel.textColor = Theme.Color.plainBlue
        
        label.text = placeholderVal.capitalized
        
        downArrow.image = #imageLiteral(resourceName: "right_arrow")
        downArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        downArrow.contentMode = .scaleAspectFit
        
        underline.backgroundColor = Theme.Color.darkerBlue
        
        addSubviews([floatingLabel, label, downArrow, underline])
        
        floatingLabelHeightConstraint = floatingLabel.heightAnchor.constraint(equalToConstant: CGFloat(floatingLabelHeight))
        gapBetweenFloatAndLabelConstraint = label.topAnchor.constraint(equalTo: floatingLabel.bottomAnchor, constant: 5)
        
        NSLayoutConstraint.activate([
            floatingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            floatingLabelHeightConstraint,
            
            gapBetweenFloatAndLabelConstraint,
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            label.trailingAnchor.constraint(equalTo: downArrow.leadingAnchor, constant: 0),
            label.bottomAnchor.constraint(equalTo: underline.topAnchor, constant: -15),
            
            downArrow.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            downArrow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            downArrow.widthAnchor.constraint(equalToConstant: 15),
            downArrow.heightAnchor.constraint(equalToConstant: 15),
            
            underline.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            underline.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            underline.heightAnchor.constraint(equalToConstant: 1),
            underline.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    fileprivate func hideFloatingLabel() {
        floatingLabelHeightConstraint.constant = 0
        gapBetweenFloatAndLabelConstraint.constant = 0
    }
    
    fileprivate func showFloatingLabel() {
        floatingLabelHeightConstraint.constant = CGFloat(floatingLabelHeight)
        gapBetweenFloatAndLabelConstraint.constant = 10
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    fileprivate func clearValue() {
        label.textColor = Theme.Color.white70
        hideFloatingLabel()
        label.text = placeholderVal.capitalized
    }
    
    func setValueAndUpdateUI(value: String?) {
        data.accept(value)
    }
}
