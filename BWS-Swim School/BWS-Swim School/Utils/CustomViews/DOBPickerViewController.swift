//
//  DOBPickerViewController.swift
//  Golf Demo
//
//  Created by PMG on 10/12/18.
//  Copyright © 2018 Nicolas Desormiere. All rights reserved.
//

import UIKit

class DOBPickerViewController: UIViewController {
    
    fileprivate let datePicker = UIDatePicker()
    
    var currentSelection: Date?
    var completionHandler: ((_ dob: Date?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if let date = currentSelection {
            datePicker.date = date
        } else {
            currentSelection = Calendar.current.date(byAdding: .year, value: -13, to: Date())
        }
    }
    
    fileprivate func setupUI() {
        view.isOpaque = false
        view.backgroundColor = Theme.Color.black25
        
        let toolBar = CustomToolbar.createBasicDoneButtonToolbar {
              self.completionHandler?(self.currentSelection)
        }
        
        datePicker.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .white
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -13, to: Date())
        
        view.addSubviews(datePicker, toolBar)
        
        NSLayoutConstraint.activate([
            toolBar.heightAnchor.constraint(equalToConstant: 44),
            toolBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            toolBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            toolBar.bottomAnchor.constraint(equalTo: datePicker.topAnchor),
            datePicker.heightAnchor.constraint(equalToConstant: 216),
            datePicker.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            datePicker.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            datePicker.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    @objc fileprivate func valueChanged(_ picker: UIDatePicker) {
        currentSelection = picker.date
    }
    
    @objc fileprivate func dismissAction(_ sender: AnyObject) {
        completionHandler?(currentSelection)
    }
}
