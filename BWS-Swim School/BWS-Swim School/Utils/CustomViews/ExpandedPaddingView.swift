//
//  ViewWithChildElementForLargerTouchArea.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 28/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

class ExpandedPaddingView: UIView {
    var viewToAddPadding: UIView = UIView()
    
    convenience init(child: UIView, frame: CGRect, childVisibleWidth: CGFloat, childVisibleHeight: CGFloat) {
        self.init(frame: frame)
        self.viewToAddPadding = child
        makeWithFixedChildDimensions(childVisibleWidth: childVisibleWidth, childVisibleHeight: childVisibleHeight)
        
    }
    
    convenience init(child: UIView, frame: CGRect, paddingHorizontal: CGFloat, paddingVertical: CGFloat) {
        self.init(frame: frame)
        self.viewToAddPadding = child
        makeWithPadding(paddingHorizontal: paddingHorizontal, paddingVertical: paddingVertical)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func makeWithFixedChildDimensions(childVisibleWidth: CGFloat, childVisibleHeight: CGFloat) {
        let view = self
        
        view.addSubview(viewToAddPadding)
        viewToAddPadding.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            viewToAddPadding.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            viewToAddPadding.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0),
            viewToAddPadding.widthAnchor.constraint(equalToConstant: childVisibleWidth),
            viewToAddPadding.heightAnchor.constraint(equalToConstant: childVisibleHeight)
        ])
        
    }
    
    private func makeWithPadding(paddingHorizontal: CGFloat, paddingVertical: CGFloat) {
        let view = self
        
        view.addSubview(viewToAddPadding)
        viewToAddPadding.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            viewToAddPadding.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: paddingHorizontal),
            viewToAddPadding.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -paddingHorizontal),
            viewToAddPadding.topAnchor.constraint(equalTo: view.topAnchor, constant: paddingVertical),
            viewToAddPadding.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -paddingVertical)
        ])
        
    }
}
