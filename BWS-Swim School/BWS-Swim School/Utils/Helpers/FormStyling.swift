//
//  FormStyling.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 31/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import SkyFloatingLabelTextField

class FormStyling {
    func styleSkyFloat(skyFloat: SkyFloatingLabelTextField) {
        skyFloat.lineColor = UIColor.blue
        skyFloat.placeholderColor = Theme.Color.white70
        skyFloat.titleColor = UIColor.white
        skyFloat.textColor = UIColor.white
        skyFloat.lineColor = Theme.Color.darkerBlue
        skyFloat.selectedTitleColor = Theme.Color.plainBlue
    }
}
