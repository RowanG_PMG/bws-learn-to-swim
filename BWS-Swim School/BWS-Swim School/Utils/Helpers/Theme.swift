//
//  Theme.swift
//  Golf Demo
//
//  Created by Nicolas Desormiere on 6/11/18.
//  Copyright © 2018 Nicolas Desormiere. All rights reserved.
//

import UIKit

class Theme {
    
    struct Color {
        static let blue = UIColor(string: "00D2FA")!
        static let darkerBlue = UIColor.rgb(red: 27, green: 170, blue: 243)
        static let plainBlue = UIColor(string: "0000ff")!
        static let green = UIColor(string: "00DC82")!
        static let orange = UIColor(string: "FFB100")!
        static let greyBackground = UIColor(string: "404955")!
        static let greyText = UIColor(string: "6A7484")!
        static let greyDisable = UIColor(string: "CBD0D8")!
        static let locatorGray = UIColor(string: "A2AEC3")!
        static let greyCellBackground = UIColor(string: "303741")!
        static let offWhiteBackground = UIColor(string: "F7F7FA")!
        static let offWhiteBackground_darker = UIColor(string: "F0F3F7")!
        static var officialApplePlaceholderGray: UIColor {
            return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        }
        static let facebookBlue = UIColor(string: "3b5998")!
        static let googleRed = UIColor(string: "DD4C3D")!
        static let onboardingRed = UIColor.rgb(red: 251, green: 32, blue: 38)
        static let onboardingBlue = UIColor.rgb(red: 10, green: 96, blue: 255)
        static let onboardingGreen = UIColor.rgb(red: 67, green: 213, blue: 82)
        static let pinkSave = UIColor(string: "FF4899")!
        static let black90 = UIColor.black.withAlphaComponent(0.90)
        static let black80 = UIColor.black.withAlphaComponent(0.80)
        static let black65 = UIColor.black.withAlphaComponent(0.65)
        static let black50 = UIColor.black.withAlphaComponent(0.50)
        static let black25 = UIColor.black.withAlphaComponent(0.25)
        static let black30 = UIColor.black.withAlphaComponent(0.30)
        static let black10 = UIColor.black.withAlphaComponent(0.10)
        
        static let white40 = UIColor.white.withAlphaComponent(0.40)
        static let white70 = UIColor.white.withAlphaComponent(0.70)
        
        static let lightOrangeNextBtn = UIColor.rgb(red: 253, green: 182, blue: 0)
        static let offYellowNextButton = UIColor.rgb(red: 254, green: 202, blue: 63)
        
    }
    
    struct CGColor {
        static let blueBorder = UIColor(string: "00C0F3")!.cgColor
        static let black80 = UIColor.black.withAlphaComponent(0.80).cgColor
        static let black65 = UIColor.black.withAlphaComponent(0.65).cgColor
        static let black50 = UIColor.black.withAlphaComponent(0.50).cgColor
        static let black45 = UIColor.black.withAlphaComponent(0.45).cgColor
        static let black10 = UIColor.black.withAlphaComponent(0.10).cgColor
        static let clear = UIColor.clear.cgColor
        static let blue = UIColor(string: "00D2FA")!.cgColor
        static let greyBackground = UIColor(string: "404955")!.cgColor
        static let greyDisable = UIColor(string: "CBD0D8")!.cgColor
        static let locatorGray = UIColor(string: "A2AEC3")!.cgColor
        static let green = UIColor(string: "00DC82")!.cgColor
        static let orange = UIColor(string: "FFB100")!.cgColor
        static let orangeBorder = UIColor(string: "FF8C00")!.cgColor
        static let pinkSave = UIColor(string: "FF4899")!.cgColor
        
    }
    
    enum FontName: String {
        case montserrat = "Montserrat"
        case roboto = "Roboto"
        case timothy = "Timothy"
    }
    
    enum FontWeight: String {
        case bold = "Bold"
        case extralight = "ExtraLight"
        case light = "Light"
        case medium = "Medium"
        case regular = "Regular"
        case semibold = "SemiBold"
    }
    
    private static func fontName(weight: FontWeight) -> String {
        return TargetManager.shared.mainFontName.rawValue + "-" + weight.rawValue
    }
    
    private static func fontName(weight: FontWeight, font: FontName = TargetManager.shared.mainFontName) -> String {
          return font.rawValue + "-" + weight.rawValue
      }
    
    private static func fontName() -> String {
        return TargetManager.shared.mainFontName.rawValue
    }
    
    struct Font {
        static func make(_ size: CGFloat, weight: FontWeight, font: FontName = TargetManager.shared.mainFontName) -> UIFont {
            let font = UIFont(name: fontName(weight: weight, font: font), size: size)
                ?? UIFont(name: fontName(weight: .regular, font: font), size: size) // fallback to same font regular weight
                ?? UIFont(name: fontName(), size: size)                 // fallback to same font no weight
                ?? UIFont.systemFont(ofSize: size)                      // fallback to system font same size
            return font
        }
        
        static func ofExtensibleSize(_ size: CGFloat, weight: FontWeight) -> UIFont {
            let extSize = size
            return make(extSize, weight: weight)
        }
    }
}
