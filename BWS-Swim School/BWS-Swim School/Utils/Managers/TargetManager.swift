//
//  TargetManager.swift
//  BWS-Swim School
//
//  Created by Rowan Louis Gontier on 25/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import UIKit

enum Target {
    case dev
}

class TargetManager {
    
    private init() {}
    static var shared = TargetManager()
    
    var mainColor: UIColor {
        switch currentTarget {
        case .dev:
            return Theme.Color.blue
        }
    }
    
    var secondColor: UIColor {
        switch currentTarget {
        case .dev:
            return Theme.Color.blue
        }
    }
    
    var mainBaseDark: UIColor {
        switch currentTarget {
        case .dev:
            return Theme.Color.blue
        }
    }
    
    var mainCgColor: CGColor {
        switch currentTarget {
        case .dev:
            return Theme.CGColor.blue
        }
    }
    
    var mainCgBorder: CGColor {
        switch currentTarget {
        case .dev:
            return Theme.CGColor.blue
        }
    }
    
    var successColor: UIColor {
        switch currentTarget {
        case .dev:
            return Theme.Color.green
        }
    }
    
    var mainFontName: Theme.FontName {
        switch currentTarget {
        case .dev:
            return .roboto
        }
    }
    
    func isCurrentTarget(_ target: Target) -> Bool {
        if let targetName = Bundle.main.object(forInfoDictionaryKey: Constants.Target.bundleName) as? String {
            if targetName == Constants.Target.dev && target == .dev {
                return true
            }
        }
        return false
    }
    
    var isDev: Bool {
        if isCurrentTarget(.dev) {
            return true
        }
        return false
    }

    var currentTarget: Target {
        if let targetName = Bundle.main.object(forInfoDictionaryKey: Constants.Target.bundleName) as? String {
            if targetName == Constants.Target.dev {
                return .dev
            }
        }
        return .dev
    }
}

