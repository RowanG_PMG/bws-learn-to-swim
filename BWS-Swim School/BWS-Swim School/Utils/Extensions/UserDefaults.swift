//
//  UserDefaults.swift
//  BWS-Swim School
//
//  Created by Rowan Louis Gontier on 25/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation

extension UserDefaults {

// MARK: - Init Demo

var hasShownOnBoardingBefore: Bool {
    get {
        return bool(forKey: Constants.UserDefaults.hasShownOnBoardingBefore)
    }
    set(value) {
        set(value, forKey: Constants.UserDefaults.hasShownOnBoardingBefore)
    }
}
}
