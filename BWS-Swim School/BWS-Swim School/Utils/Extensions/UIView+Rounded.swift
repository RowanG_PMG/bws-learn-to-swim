//
//  UIView+Rounded.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 30/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

extension UIView {
    func makeRounded() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
