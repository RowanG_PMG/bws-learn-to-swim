//
//  UIStackView.swift
//  Golf Demo
//
//  Created by Nicolas Desormiere on 6/11/18.
//  Copyright © 2018 Nicolas Desormiere. All rights reserved.
//

import UIKit

extension UIStackView {
    
    public func addArrangedSubviews(_ views: [UIView]) {
        views.forEach { (subview) in
            addArrangedSubview(subview)
            subview.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
