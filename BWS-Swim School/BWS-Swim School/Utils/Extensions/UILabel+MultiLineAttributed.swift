//
//  UILabel+MultiLineAttributed.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 30/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

extension UILabel {
    func makeMultiLineCentred () {
        self.numberOfLines = 0
        self.textAlignment = .center
        
    }
}
