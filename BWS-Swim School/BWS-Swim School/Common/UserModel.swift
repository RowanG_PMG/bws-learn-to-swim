//
//  UserModel.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 18/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation

protocol UserModelProtocol {
    func getSwimExperienceOptions() -> Array<String>
}

class MockUserModel: UserModelProtocol {
    func getSwimExperienceOptions() -> Array<String> {
        return ["New to swimming", "6 months to 1 year practice", "Up to 2 years practice"]
    }
}
