//
//  Constants.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 25/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation

struct Constants {
    static let emptyString = ""
    static let continueBtnLabel = "Continue"
    
    struct CreateChildProfile {
        static let swimExperienceError = "Please choose swim experience"
        static let childNameError = "Please enter a valid Name"
        static let dateBirthError = "Please enter a valid Date of Birth"
        
        static let childNameFormLabel = "Child's Name"
        static let dateBirthFormLabel = "Date of birth"
        static let swimExperienceFormLabel = "Swim experience"
        
        static let createChildHeadingTop = "Tell us about your child"
        static let createChildHeadingBottom = "To personalise their journey & pick the right course for them."
        
        static let needToCaptureUserData = "*This info will determine the best course for your child/children."
        
        static let minimumChildNameLength = 2
        static let maximumChildNameLength = 80
    }
    
    struct UserDefaults {
        static let hasShownOnBoardingBefore = "hasShownOnBoardingBefore"
        
    }
    
    struct Target {
        static let dev = "Dev"
        static let bundleName = "CFBundleName"
    }
}
