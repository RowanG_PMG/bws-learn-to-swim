//
//  GettingStartedViewController.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 29/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit
import Foundation
import GestureRecognizerClosures

class GettingStartedViewController: CompactSizePortraitOnlyViewController {
    fileprivate let topLogoImage = UIImageView()
    fileprivate let spacerBetweenLogoAndText = UIView()
    fileprivate let middleText = UILabel()
    fileprivate let bottomStripButton = BottomStripButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    fileprivate func setupUI() {
        
        self.view.backgroundColor = Theme.Color.blue
        
        topLogoImage.image = #imageLiteral(resourceName: "learn_to_swim_icon")
        topLogoImage.contentMode = .scaleAspectFit
        
        middleText.makeMultiLineCentred()
        middleText.attributedText = AttributedTextOnboardingHelper().makeAttributedTopText(topText: "Learn to swim", bottomText: "Teach your kids how to swim")
        
        bottomStripButton.nextTitle.text = "Get started"
        bottomStripButton.onTap {
            gesture in
            self.getStartedTapped()
        }
        
        self.view.addSubviews([topLogoImage, spacerBetweenLogoAndText, middleText, bottomStripButton])
        
        NSLayoutConstraint.activate([
            topLogoImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 130),
            topLogoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            topLogoImage.widthAnchor.constraint(lessThanOrEqualTo: view.widthAnchor, multiplier: 0.3),
            topLogoImage.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, multiplier: 0.2),
            
            spacerBetweenLogoAndText.topAnchor.constraint(lessThanOrEqualTo: topLogoImage.bottomAnchor),
            spacerBetweenLogoAndText.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, multiplier: 0.1),
            
            middleText.topAnchor.constraint(equalTo: spacerBetweenLogoAndText.bottomAnchor, constant: 0),
            middleText.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
            middleText.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10),
            middleText.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.22),
            
            bottomStripButton.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomStripButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            bottomStripButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            bottomStripButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.09)
        ])
        
    }
    
    fileprivate func getStartedTapped() {
        let window =  (UIApplication.shared.delegate as! AppDelegate).window
        window?.rootViewController =  OnBoardingViewController()
    }

}
