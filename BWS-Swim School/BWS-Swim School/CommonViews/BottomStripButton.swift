//
//  BottomStripButton.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 29/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

class BottomStripButton: UIView {
    let nextTitle = UILabel()
    fileprivate let nextAreaContainerOfArrow = UIView()
    fileprivate let nextArrow = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    convenience init (label: String) {
        self.init()
        nextTitle.text = label
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setup() {
        isUserInteractionEnabled = true
        backgroundColor = Theme.Color.offYellowNextButton
        
        nextTitle.font = Theme.Font.make(34, weight: Theme.FontWeight.regular, font: Theme.FontName.timothy)
        nextTitle.textColor = UIColor.white
        nextTitle.textAlignment = .center
        
        nextAreaContainerOfArrow.backgroundColor = Theme.Color.lightOrangeNextBtn
        nextArrow.image = #imageLiteral(resourceName: "right_arrow")
        
        addSubviews(nextTitle, nextAreaContainerOfArrow, nextArrow)
        
        NSLayoutConstraint.activate([
            nextTitle.topAnchor.constraint(equalTo: topAnchor),
            nextTitle.bottomAnchor.constraint(equalTo: bottomAnchor),
            nextTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            nextTitle.trailingAnchor.constraint(equalTo: nextAreaContainerOfArrow.leadingAnchor, constant: 0),
            
            nextAreaContainerOfArrow.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            nextAreaContainerOfArrow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            nextAreaContainerOfArrow.widthAnchor.constraint(equalToConstant: 100),
            nextAreaContainerOfArrow.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            nextArrow.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            nextArrow.centerXAnchor.constraint(equalTo: nextAreaContainerOfArrow.centerXAnchor, constant: 0),
            nextArrow.widthAnchor.constraint(equalToConstant: 20),
            nextArrow.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
}
