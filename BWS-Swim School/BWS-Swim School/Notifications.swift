//
//  Notifications.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 29/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation

enum Notifications {
    static let OnboardingCompletedNotification = "OnboardingCompletedNotification"
}
