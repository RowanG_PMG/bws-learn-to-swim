//
//  OnBoardingViewController.swift
//
//  Created by PMG Media on 22/3/19.
//  Copyright © 2019 Nicolas Desormiere/Rowan Gontier. All rights reserved.
//

import UIKit

protocol OnBoardingViewControllerDelegate: AnyObject {
    func closeOnBoarding(_ viewController: UIViewController)
}

typealias OnBoardingCellModel = (topBigText: String, topSmallText: String,image: UIImage, color: UIColor, title: String, bottomWavyImage: UIImage)

class OnBoardingViewController: CompactSizePortraitOnlyViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    fileprivate let skipButton = UIButton()
    fileprivate var prevButton: ExpandedPaddingView!
    fileprivate let nextButtonContainer = BottomStripButton()
    fileprivate let topLogoImage = UIImageView()
    fileprivate var collectionView: UICollectionView!
    fileprivate let loginPageControl = UIPageControl()
    fileprivate let datasource =
        [OnBoardingCellModel(topBigText: "Tell us about your child", topSmallText: "Personalise your content",image: #imageLiteral(resourceName: "activities_onboarding_icon"), color: Theme.Color.onboardingRed, title: "EXPLORE", #imageLiteral(resourceName: "onboarding_bottom_shade_1")),
         OnBoardingCellModel(topBigText: "Enjoy free teaching videos", topSmallText: "Explore lesson plans or activities", image:  #imageLiteral(resourceName: "tell_us_about_child_onboarding"), color: Theme.Color.onboardingBlue, title: "LOCATE", #imageLiteral(resourceName: "onboarding_bottom_shade_2")),
         OnBoardingCellModel(topBigText: "Unlock lesson plans", topSmallText: "Unlock lesson plans suitable for your child", image:  #imageLiteral(resourceName: "unlock_icon_onboarding"), color: Theme.Color.onboardingGreen, title: "PARTICIPATE", #imageLiteral(resourceName: "onboarding_bottom_shade_3"))
    ]
    
    weak var delegate: OnBoardingViewControllerDelegate?
    
    private var currentIndex = 0 {
        didSet {
            loginPageControl.currentPage = currentIndex
            nextButtonContainer.nextTitle.text = currentIndex == datasource.count - 1 ? "CLOSE" : "NEXT"
            prevButton.isHidden = currentIndex == 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        registerCells()
    }
    
    override func viewDidLayoutSubviews() {
        loginPageControl.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    }
    
    fileprivate func setupUI() {
        topLogoImage.image = #imageLiteral(resourceName: "learn_to_swim_icon")
        topLogoImage.contentMode = .scaleAspectFit
        
        let prevButtonContent = UIImageView()
        prevButtonContent.image = #imageLiteral(resourceName: "right_arrow")
        prevButton = ExpandedPaddingView(child: prevButtonContent, frame: CGRect.zero, childVisibleWidth: 20, childVisibleHeight: 20)
        let previousTap = UITapGestureRecognizer(target: self, action: #selector(self.prevTapped(_:)))
        prevButton.addGestureRecognizer(previousTap)
        prevButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        skipButton.setTitle("SKIP", for: .normal)
        skipButton.titleLabel?.font = Theme.Font.make(26, weight: Theme.FontWeight.regular, font: Theme.FontName.timothy)
        skipButton.setTitleColor(Theme.Color.black50, for: UIControl.State.normal)
        skipButton.addTarget(self, action: #selector(skipTapped), for: .touchUpInside)
        
        let collectionLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionLayout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        collectionView.bounces = false
        collectionView.showsHorizontalScrollIndicator = false
        
        loginPageControl.numberOfPages = datasource.count
        
        let nextTap = UITapGestureRecognizer(target: self, action: #selector(self.nextTapped(_:)))
        nextButtonContainer.addGestureRecognizer(nextTap)
        
        view.addSubviews(collectionView, topLogoImage, loginPageControl, skipButton, prevButton, nextButtonContainer)
        
        NSLayoutConstraint.activate([
            skipButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            skipButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            skipButton.heightAnchor.constraint(equalToConstant: 28),
            
            topLogoImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            topLogoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            topLogoImage.widthAnchor.constraint(lessThanOrEqualTo: view.widthAnchor, multiplier: 0.11),
            topLogoImage.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, multiplier: 0.11),
            
            prevButton.centerYAnchor.constraint(equalTo: skipButton.centerYAnchor, constant: 0),
            prevButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            prevButton.widthAnchor.constraint(equalToConstant: 30),
            prevButton.heightAnchor.constraint(equalToConstant: 30),
            
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: nextButtonContainer.topAnchor, constant: 0),
            
            loginPageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginPageControl.bottomAnchor.constraint(equalTo: nextButtonContainer.topAnchor, constant: 0),
            
            nextButtonContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            nextButtonContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            nextButtonContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            nextButtonContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.09)
            
        ])
        
        currentIndex = 0
        collectionView.reloadData()
    }
    
    fileprivate func registerCells() {
        collectionView?.register(OnBoardingCollectionViewCell.self, forCellWithReuseIdentifier: OnBoardingCollectionViewCell.nameOfClass)
    }
    
    // MARK: - UICollectionViewDelegate & UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnBoardingCollectionViewCell.nameOfClass, for: indexPath) as! OnBoardingCollectionViewCell
        cell.configureCell(datasource[indexPath.row])
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.currentIndex = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    @objc fileprivate func skipTapped() {
        UserDefaults.standard.hasShownOnBoardingBefore = true
        NotificationCenter.default.post(name: Notification.Name(rawValue: Notifications.OnboardingCompletedNotification), object: self)
    }
    
    @objc fileprivate func prevTapped(_ sender: UITapGestureRecognizer) {
        if currentIndex >= 1 {
            collectionView.scrollToItem(at: IndexPath(item: currentIndex - 1, section: 0), at: .centeredHorizontally, animated: true)
            currentIndex -= 1
        }
    }
    
    @objc fileprivate func nextTapped(_ sender: UITapGestureRecognizer) {
        if currentIndex == datasource.count - 1 {
            skipTapped()
        } else if currentIndex < datasource.count - 1 {
            collectionView.scrollToItem(at: IndexPath(item: currentIndex + 1, section: 0), at: .centeredHorizontally, animated: true)
            currentIndex += 1
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animateAlongsideTransition(in: nil, animation: nil, completion: {
            _ in
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: self.currentIndex, section: 0), at: .centeredHorizontally, animated: false)
        })
        
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
        } else {
            print("Portrait")
        }
    }
    
    
}
