//
//  AttributedTextHelper.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 28/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import  UIKit

class AttributedTextOnboardingHelper {
    func makeAttributedTopText(topText: String, bottomText: String) -> NSAttributedString {
        let upperTextAttributes: [NSAttributedString.Key: Any] = [.font: Theme.Font.make(32, weight: Theme.FontWeight.bold, font: Theme.FontName.roboto), NSAttributedString.Key.foregroundColor: UIColor.white]
        let lowerTextAttributes : [NSAttributedString.Key: Any] = [.font: Theme.Font.make(18, weight: Theme.FontWeight.bold, font: Theme.FontName.roboto), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let completeString = NSMutableAttributedString()
        
        let upperTextString = NSMutableAttributedString(string: "\(topText)", attributes: upperTextAttributes)
        let lowerTextString = NSAttributedString(string: "\n______\n\n\(bottomText)", attributes: lowerTextAttributes)
        
        completeString.append(upperTextString)
        completeString.append(lowerTextString)
        return completeString
    }
}
