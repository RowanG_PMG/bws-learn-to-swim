//
//  OnBoardingCollectionViewCell.swift
//  Golf Demo
//
//  Created by PMG Media on 25/3/19.
//  Copyright © 2019 Nicolas Desormiere. All rights reserved.
//

import UIKit

class OnBoardingCollectionViewCell: UICollectionViewCell {
    fileprivate let topSpacerGuide = UIView()
    fileprivate let slideText = UILabel()
    fileprivate let centreMainImageView = UIImageView()
    fileprivate let bottomWavyImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        centreMainImageView.contentMode = .scaleAspectFit
        
        slideText.makeMultiLineCentred()
        slideText.textColor = .white
        
        slideText.font = Theme.Font.ofExtensibleSize(18, weight: .regular)
        
        addSubviews(topSpacerGuide, bottomWavyImage, centreMainImageView, slideText)
        
        NSLayoutConstraint.activate([
            topSpacerGuide.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            topSpacerGuide.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.21),
            
            slideText.topAnchor.constraint(equalTo: topSpacerGuide.bottomAnchor, constant: 0),
            slideText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            slideText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            slideText.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.22),
            
            centreMainImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            centreMainImageView.widthAnchor.constraint(lessThanOrEqualTo: widthAnchor, multiplier: 0.8),
            centreMainImageView.topAnchor.constraint(equalTo: slideText.bottomAnchor, constant: 10),
            centreMainImageView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -10),
            
            bottomWavyImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            bottomWavyImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            bottomWavyImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            bottomWavyImage.heightAnchor.constraint(lessThanOrEqualTo: heightAnchor, multiplier: 0.3)
        ])
        
    }
    
    func configureCell(_ model: OnBoardingCellModel) {
        contentView.backgroundColor = model.color
        
        slideText.attributedText = AttributedTextOnboardingHelper().makeAttributedTopText(topText: model.topBigText, bottomText: model.topSmallText)
        
        centreMainImageView.image = model.image
        centreMainImageView.layer.shadowColor = Theme.CGColor.black65
        centreMainImageView.layer.shadowOpacity = 1
        centreMainImageView.layer.shadowOffset = .zero
        centreMainImageView.layer.shadowRadius = 10
        
        bottomWavyImage.image = model.bottomWavyImage
    }
    
}
