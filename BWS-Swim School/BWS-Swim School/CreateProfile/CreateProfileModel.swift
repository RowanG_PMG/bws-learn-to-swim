//
//  CreateProfileModel.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 31/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation

protocol CreateProfileModelProtocol {
    func addChildProfile(name: String, dateOfBirth: String, swimExperience: String, completionHandler: (String, Error?) -> Void)
}

class CreateProfileModel: CreateProfileModelProtocol {
     
    func addChildProfile(name: String, dateOfBirth: String, swimExperience: String, completionHandler: (String, Error?) -> Void) {
        //Mock successful add child profile entry- temporary
        completionHandler("Successfully added child profile", nil)
    }
}
