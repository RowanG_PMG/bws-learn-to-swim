//
//  CreateProfileViewModel.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 31/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CreateProfileViewModel {
    
    fileprivate let disposebag = DisposeBag()
    fileprivate var createProfileModel: CreateProfileModel?
    
    // Initialise ViewModel's
    var childNameViewModel: ValidationViewModel?
    var dateOfBirthViewModel: ValidationViewModel?
    var swimExperienceViewModel: ValidationViewModel?
    
    // Fields that bind to our view's
    let isSuccess : BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let isLoading : BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMsg : BehaviorRelay<String> = BehaviorRelay(value: Constants.emptyString)
    
    init(createProfileModel: CreateProfileModel, childNameViewModel: ValidationViewModel = ChildNameViewModel(), dateOfBirthViewModel: ValidationViewModel = DateOfBirthViewModel(), swimExperienceViewModel: ValidationViewModel = SwimExperienceViewModel()) {
        self.createProfileModel = createProfileModel
        self.childNameViewModel = childNameViewModel
        self.dateOfBirthViewModel = dateOfBirthViewModel
        self.swimExperienceViewModel = swimExperienceViewModel
    }
    
    func validateCredentials() -> Bool {
        let childValid = childNameViewModel?.validateCredentials()
        let dateBirthValid = dateOfBirthViewModel?.validateCredentials()
        let swimValid = swimExperienceViewModel?.validateCredentials()
        
        return (childValid ?? false) && (dateBirthValid ?? false) && (swimValid ?? false)
    }
    
   fileprivate func createProfileChildCompletionHandler() -> ((String, Error?) -> Void) {
        return CompletionBlock<String>().make(onResult: { [weak self] string in
                print("\(string)")
                }, onError: { [weak self] error in
                    print("Error occurred.")
                }
        )
    }
    
    func createProfilePressed() {
        if (validateCredentials() == true) {
            if let childNameVal = childNameViewModel?.data.value, let dateBirthVal = dateOfBirthViewModel?.data.value!, let swimExperienceVal = swimExperienceViewModel?.data.value {
                createProfileModel?.addChildProfile(name: childNameVal, dateOfBirth: dateBirthVal, swimExperience: swimExperienceVal, completionHandler: createProfileChildCompletionHandler())
            } else {
                print("form entry not valid")
            }
        }
    }
}
