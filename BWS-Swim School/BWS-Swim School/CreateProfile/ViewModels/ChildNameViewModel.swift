//
//  ChldeNameViewModel.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 31/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ChildNameViewModel: ValidationViewModel {
    var errorMessage: String = Constants.CreateChildProfile.childNameError
    var data: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    var errorValue: BehaviorRelay<String?> = BehaviorRelay(value: Constants.emptyString)
    
    func validateCredentials() -> Bool {
        let minChildNameLength = Constants.CreateChildProfile.minimumChildNameLength
        let maxChildNameLength = Constants.CreateChildProfile.maximumChildNameLength
        
        guard validateLength(text: data.value, size: (minChildNameLength, maxChildNameLength)) else {
            errorValue.accept(errorMessage)
            return false;
        }
        
        errorValue.accept(Constants.emptyString)
        return true
    }
    
   fileprivate func validateLength(text : String?, size : (min : Int, max : Int)) -> Bool {
        if let text = text {
              return (size.min...size.max).contains(text.count)
        } else {
            return false
        }
    }
}
