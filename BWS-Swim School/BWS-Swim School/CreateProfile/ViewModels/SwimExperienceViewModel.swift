//
//  SwimExperienceViewModel.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 19/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import RxSwift
import  RxCocoa

class SwimExperienceViewModel: ValidationViewModel {
    var errorMessage: String = Constants.CreateChildProfile.swimExperienceError
    
    var data: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    var errorValue: BehaviorRelay<String?> = BehaviorRelay(value: Constants.emptyString)
    
    func validateCredentials() -> Bool {
        guard validate(text: data.value) else {
            errorValue.accept(errorMessage)
            return false
        }
        
        errorValue.accept(Constants.emptyString)
        return true
    }
    
    fileprivate func validate(text : String?) -> Bool {
        
        guard let _ = text else {
            return false
        }
        
        return (text != Constants.emptyString)
    }
}
