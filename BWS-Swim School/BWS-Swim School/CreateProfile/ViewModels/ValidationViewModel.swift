//
//  ValidationViewModel.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 31/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import  UIKit
import  RxSwift
import RxCocoa

protocol ValidationViewModel {
    var errorMessage: String { get }
    
    // Observables
    var data: BehaviorRelay<String?> { get set }
    var errorValue: BehaviorRelay<String?> { get}
    
    // Validation
    func validateCredentials() -> Bool
}
