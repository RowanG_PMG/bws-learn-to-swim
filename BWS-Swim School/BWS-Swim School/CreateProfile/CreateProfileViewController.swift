//
//  CreateProfileViewController.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 30/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SkyFloatingLabelTextField

class CreateProfileViewController: CompactSizePortraitOnlyViewController {
    fileprivate let containerScrollView = UIScrollView()
    fileprivate let scrollContentView = UIView()
    fileprivate let topLogoImage = UIImageView()
    fileprivate let spacerBetweenLogoAndText = UIView()
    fileprivate let descriptorText = UILabel()
    fileprivate var userLogoImageViewContainer: ExpandedPaddingView!
    fileprivate let userLogoImageView = UIImageView()
    fileprivate let formFieldContainer = UIStackView()
    fileprivate var childNameSkyField: SkyFloatingLabelTextField!
    fileprivate var dateBirthField = FloatPickerField(placeholder: Constants.CreateChildProfile.dateBirthFormLabel)
    fileprivate var swimExperienceField = FloatPickerField(placeholder: Constants.CreateChildProfile.swimExperienceFormLabel)
    fileprivate let reasonForDataCaptureLabel = UILabel()
    fileprivate let continueButtonContainer = BottomStripButton(label: Constants.continueBtnLabel)
    
    fileprivate var pickerdob: Date?
    
    var viewModel = CreateProfileViewModel(createProfileModel: CreateProfileModel())
    fileprivate  let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        setupUI()
        doBindingsFromFormFieldEntryToVM()
        doBindingsFromViewModelFormErrorToInputFields()
        setupReactiveDebounceValidation()
    }
    
    fileprivate func setupUI() {
        self.view.backgroundColor = Theme.Color.blue
        
        topLogoImage.image = #imageLiteral(resourceName: "learn_to_swim_icon")
        topLogoImage.contentMode = .scaleAspectFit
        
        setupUserLogo()
        
        descriptorText.attributedText = AttributedTextOnboardingHelper().makeAttributedTopText(topText: Constants.CreateChildProfile.createChildHeadingTop, bottomText: Constants.CreateChildProfile.createChildHeadingBottom)
        descriptorText.makeMultiLineCentred()
        
        setupFormFieldStackView()
        
        setupChildNameField()
        
        addDateBirthFieldTapHandling()
        addSwimExperienceFieldTapHandling()
        
        setupReasonForDataCaptureLabel()
        
        addScrollViewAndBottomButtonToView()
        addContentParentToScrollView()
        addScrollViewInnerContent()
        addFormFieldsToStackView()
        
        continueButtonContainer.onTap {
            gesture in
            self.viewModel.createProfilePressed()
        }
    }
    
    fileprivate  func setupFormFieldStackView() {
        formFieldContainer.axis = .vertical
        formFieldContainer.distribution = .fill
        formFieldContainer.alignment = .fill
        formFieldContainer.spacing = 8
    }
    
    fileprivate func setupChildNameField() {
        childNameSkyField = SkyFloatingLabelTextField(frame: CGRect.zero)
        childNameSkyField.placeholder = Constants.CreateChildProfile.childNameFormLabel
        childNameSkyField.title = Constants.CreateChildProfile.childNameFormLabel
        FormStyling().styleSkyFloat(skyFloat: childNameSkyField)
    }
    
    fileprivate  func setupReasonForDataCaptureLabel() {
        reasonForDataCaptureLabel.text = Constants.CreateChildProfile.needToCaptureUserData
        reasonForDataCaptureLabel.numberOfLines = 0
        reasonForDataCaptureLabel.font = Theme.Font.make(11, weight: Theme.FontWeight.regular, font: Theme.FontName.roboto)
        reasonForDataCaptureLabel.textColor = UIColor.white
    }
    
    fileprivate func setupUserLogo() {
        let userLogoPadding = CGFloat(16)
        userLogoImageViewContainer = ExpandedPaddingView(child: userLogoImageView, frame: CGRect.zero, paddingHorizontal: userLogoPadding, paddingVertical: userLogoPadding)
        userLogoImageViewContainer.contentMode = .scaleAspectFit
        userLogoImageView.image = #imageLiteral(resourceName: "profile_icon")
        userLogoImageViewContainer.backgroundColor = Theme.Color.darkerBlue
    }
    
    fileprivate func addDateBirthFieldTapHandling() {
        dateBirthField.onTap {
            gesture in
            
            let dobPickerViewController = DOBPickerViewController()
            dobPickerViewController.modalTransitionStyle = .crossDissolve
            dobPickerViewController.modalPresentationStyle = .overFullScreen
            if let date = self.pickerdob {
                dobPickerViewController.currentSelection = date
            }
            dobPickerViewController.completionHandler = { [weak self] dob in
                guard let `self` = self else { return }
                self.pickerdob = dob
                if let date = dob {
                    let dateString = DateFormatter.ddMMyy.string(from: date)
                    self.dateBirthField.setValueAndUpdateUI(value: dateString)
                }
                self.dismiss(animated: true, completion: nil)
            }
            self.present(dobPickerViewController, animated: true, completion: nil)
        }
    }
    
    fileprivate  func addSwimExperienceFieldTapHandling() {
        swimExperienceField.onTap {
            gesture in
            let swimExperiencePickerViewController = SwimExperienceViewController()
            swimExperiencePickerViewController.modalTransitionStyle = .crossDissolve
            swimExperiencePickerViewController.modalPresentationStyle = .overFullScreen
            
            swimExperiencePickerViewController.completionHandler = {  [weak self] experience in
                guard let `self` = self else { return }
                if let experience = experience {
                    self.swimExperienceField.setValueAndUpdateUI(value: experience)
                }
                self.dismiss(animated: true, completion: nil)
            }
            
            self.present(swimExperiencePickerViewController, animated: true, completion: nil)
        }
    }
    
    fileprivate func addScrollViewAndBottomButtonToView() {
        self.view.addSubviews([containerScrollView, continueButtonContainer])
        NSLayoutConstraint.activate([
            containerScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            containerScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerScrollView.bottomAnchor.constraint(equalTo: continueButtonContainer.topAnchor),
            continueButtonContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            continueButtonContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            continueButtonContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            continueButtonContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.09)
        ])
    }
    
    fileprivate func addContentParentToScrollView() {
        containerScrollView.addSubviews([scrollContentView])
        
        let heightConstraintScrollContent = scrollContentView.heightAnchor.constraint(equalTo: containerScrollView.heightAnchor)
        heightConstraintScrollContent.priority = .defaultLow
        
        NSLayoutConstraint.activate([
            scrollContentView.topAnchor.constraint(equalTo: containerScrollView.topAnchor),
            scrollContentView.bottomAnchor.constraint(equalTo: containerScrollView.bottomAnchor),
            scrollContentView.leadingAnchor.constraint(equalTo: containerScrollView.leadingAnchor),
            scrollContentView.trailingAnchor.constraint(equalTo: containerScrollView.trailingAnchor),
            scrollContentView.widthAnchor.constraint(equalTo: containerScrollView.widthAnchor),
            heightConstraintScrollContent
        ])
    }
    
    fileprivate func addScrollViewInnerContent() {
        
        scrollContentView.addSubviews([topLogoImage, spacerBetweenLogoAndText,descriptorText, userLogoImageViewContainer, formFieldContainer, reasonForDataCaptureLabel])
        
        NSLayoutConstraint.activate([
            topLogoImage.topAnchor.constraint(equalTo: scrollContentView.topAnchor, constant: 10),
            topLogoImage.centerXAnchor.constraint(equalTo: scrollContentView.centerXAnchor),
            topLogoImage.widthAnchor.constraint(lessThanOrEqualTo: scrollContentView.widthAnchor, multiplier: 0.25),
            topLogoImage.heightAnchor.constraint(lessThanOrEqualTo: scrollContentView.heightAnchor, multiplier: 0.08),
            
            spacerBetweenLogoAndText.topAnchor.constraint(lessThanOrEqualTo: topLogoImage.bottomAnchor),
            spacerBetweenLogoAndText.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, multiplier: 0),
            
            descriptorText.topAnchor.constraint(equalTo: spacerBetweenLogoAndText.bottomAnchor, constant: 0),
            descriptorText.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor, constant: 10),
            descriptorText.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor, constant: -10),
            descriptorText.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.23),
            
            userLogoImageViewContainer.topAnchor.constraint(equalTo: descriptorText.bottomAnchor, constant: 10),
            userLogoImageViewContainer.centerXAnchor.constraint(equalTo: scrollContentView.centerXAnchor),
            userLogoImageViewContainer.widthAnchor.constraint(lessThanOrEqualTo: scrollContentView.widthAnchor, multiplier: 0.2),
            userLogoImageViewContainer.heightAnchor.constraint(lessThanOrEqualTo: userLogoImageViewContainer.widthAnchor, multiplier: 1),
            
            formFieldContainer.topAnchor.constraint(equalTo: userLogoImageViewContainer.bottomAnchor, constant: 10),
            formFieldContainer.centerXAnchor.constraint(equalTo: scrollContentView.centerXAnchor),
            formFieldContainer.widthAnchor.constraint(equalTo: scrollContentView.widthAnchor, multiplier: 0.9),
            
            reasonForDataCaptureLabel.topAnchor.constraint(equalTo: formFieldContainer.bottomAnchor, constant: 10),
            reasonForDataCaptureLabel.centerXAnchor.constraint(equalTo: scrollContentView.centerXAnchor),
            reasonForDataCaptureLabel.widthAnchor.constraint(equalTo: scrollContentView.widthAnchor, multiplier: 0.9),
            reasonForDataCaptureLabel.bottomAnchor.constraint(equalTo: scrollContentView.bottomAnchor, constant: -10),
            reasonForDataCaptureLabel.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    fileprivate func addFormFieldsToStackView() {
        formFieldContainer.addArrangedSubviews([childNameSkyField, dateBirthField, swimExperienceField])
        NSLayoutConstraint.activate([
            childNameSkyField.heightAnchor.constraint(equalToConstant: 70),
            dateBirthField.heightAnchor.constraint(equalToConstant: 70),
            swimExperienceField.heightAnchor.constraint(equalToConstant: 70)
        ])
    }
    
    fileprivate func doBindingsFromViewModelFormErrorToInputFields() {
        bindChildNameVMErrorToChildNameField()
        bindDateBirthVMErrorToDateBirthField()
        bindSwimExperienceVMErrorToSwimExperienceField()
    }
    
    fileprivate func doBindingsFromFormFieldEntryToVM() {
        bindChildNameTextFieldToVM()
        bindDateBirthFieldToVM()
        bindSwimExperienceFieldToVM()
    }
    
    fileprivate func setupReactiveDebounceValidation() {
        debounceValidationForChildName()
        debounceValidationForDateOfBirth()
        debounceValidationForSwimExperience()
    }
    
    fileprivate func bindChildNameTextFieldToVM () {
        childNameSkyField.rx.text.orEmpty
            .bind(to: viewModel.childNameViewModel!.data)
            .disposed(by: disposeBag)
    }
    
    fileprivate func debounceValidationForChildName() {
        childNameSkyField.rx.text.orEmpty.skip(2).debounce(.milliseconds(1300), scheduler: MainScheduler.instance).subscribe(onNext: { value in
            self.viewModel.childNameViewModel?.validateCredentials()
        }).disposed(by: disposeBag)
    }
    
    fileprivate func bindChildNameVMErrorToChildNameField() {
        self.viewModel.childNameViewModel!.errorValue.asObservable().map { state -> String in
            return state ?? Constants.emptyString
        }.subscribe(onNext: { error in
            self.childNameSkyField.errorMessage = error }).disposed(by: disposeBag)
    }
    
    fileprivate func bindDateBirthFieldToVM () {
        dateBirthField.data.asObservable()
            .map { text -> String? in
                return text
        }
        .bind(to:self.viewModel.dateOfBirthViewModel!.data)
        .disposed(by:self.disposeBag)
    }
    
    fileprivate func bindDateBirthVMErrorToDateBirthField() {
        self.viewModel.dateOfBirthViewModel!.errorValue.asObservable().map { state -> String in
            return state ?? Constants.emptyString
        }.bind(to: self.dateBirthField.errorVal).disposed(by: disposeBag)
    }
    
    fileprivate func debounceValidationForDateOfBirth() {
        dateBirthField.data.asObservable().skip(1).subscribe(onNext: { value in
            self.viewModel.dateOfBirthViewModel?.validateCredentials()
        }).disposed(by: disposeBag)
    }
    
    fileprivate func bindSwimExperienceFieldToVM () {
        swimExperienceField.data.asObservable()
            .map { text -> String? in
                return text
        }
        .bind(to:self.viewModel.swimExperienceViewModel!.data)
        .disposed(by:self.disposeBag)
    }
    
    fileprivate func bindSwimExperienceVMErrorToSwimExperienceField() {
        self.viewModel.swimExperienceViewModel!.errorValue.asObservable().map { state -> String in
            return state ?? Constants.emptyString
        }.bind(to: self.swimExperienceField.errorVal).disposed(by: disposeBag)
    }
    
    fileprivate func debounceValidationForSwimExperience() {
        swimExperienceField.data.asObservable().skip(1).subscribe(onNext: { value in
            self.viewModel.swimExperienceViewModel?.validateCredentials()
        }).disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews() {
        userLogoImageViewContainer.makeRounded()
    }
}
