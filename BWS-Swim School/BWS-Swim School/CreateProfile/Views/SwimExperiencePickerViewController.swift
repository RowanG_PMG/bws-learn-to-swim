//
//  SwimExperiencePickerViewController.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 18/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

class SwimExperienceViewController: UIViewController
{
    
    fileprivate let swimExperiencePicker = SwimExperiencePickerView()
    fileprivate var swimExperienceModel = MockUserModel()
    
    var currentSelection: String?
    var completionHandler: ((_ swimExperience: String?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPickerData()
        setupUI()
    }
    
    fileprivate func setupPickerData() {
        let swimExperienceOptions = swimExperienceModel.getSwimExperienceOptions()
        swimExperiencePicker.data = swimExperienceOptions
        currentSelection = swimExperienceOptions.first
    }
    
    fileprivate func setupUI() {
        view.isOpaque = false
        view.backgroundColor = Theme.Color.black25
        
        swimExperiencePicker.backgroundColor = .white
        swimExperiencePicker.swimExperienceDelegate = self
        
        let toolBar = createPickerToolbar()
        view.addSubviews(swimExperiencePicker, toolBar)
        
        NSLayoutConstraint.activate([
            toolBar.heightAnchor.constraint(equalToConstant: 44),
            toolBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            toolBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            toolBar.bottomAnchor.constraint(equalTo: swimExperiencePicker.topAnchor),
            swimExperiencePicker.heightAnchor.constraint(equalToConstant: 216),
            swimExperiencePicker.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            swimExperiencePicker.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            swimExperiencePicker.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
   fileprivate func createPickerToolbar() -> CustomToolbar {
        return CustomToolbar.createBasicDoneButtonToolbar {
            self.completionHandler?(self.currentSelection)
        }
    }
    
    @objc fileprivate func dismissAction(_ sender: AnyObject) {
        completionHandler?(currentSelection)
    }
}

extension SwimExperienceViewController: SwimExperienceDelegate {
    func pickerItemSelected(item: String) {
        currentSelection = item
    }
}
