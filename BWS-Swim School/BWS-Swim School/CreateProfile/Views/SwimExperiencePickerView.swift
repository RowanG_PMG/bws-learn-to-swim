//
//  SwimExperiencePickerView.swift
//  Dev-BWS-Swim School
//
//  Created by Rowan Louis Gontier on 18/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

protocol SwimExperienceDelegate {
   func pickerItemSelected(item: String)
}

class SwimExperiencePickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    var swimExperienceDelegate: SwimExperienceDelegate? = nil
    
    public var data: [String]? {
        didSet {
            super.delegate = self
            super.dataSource = self
            self.reloadAllComponents()
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if data != nil {
            return data!.count
        } else {
            return 0
        }
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if data != nil {
            return data![row]
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        swimExperienceDelegate?.pickerItemSelected(item: data![row])
    }
    
    public var selectedValue: String {
        get {
            if data != nil {
                return data![selectedRow(inComponent: 0)]
            } else {
                return ""
            }
        }
    }
}
