//
//  AppDelegate.swift
//  BWS-Swim School
//
//  Created by Rowan Louis Gontier on 25/10/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        showOnBoardingIfNeeded() // OnBoarding or MainTabBar
        subscribeToNotifications()
        window?.makeKeyAndVisible()
        // printAllFontsAvailable()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    fileprivate func printAllFontsAvailable() {
        for family in UIFont.familyNames.sorted() {
            let names = UIFont.fontNames(forFamilyName: family)
            print("Family: \(family) Font names: \(names)")
        }
    }
    
    fileprivate func showOnBoardingIfNeeded() {
        if !UserDefaults.standard.hasShownOnBoardingBefore {
            window?.rootViewController = GettingStartedViewController()
        } else {
            window?.rootViewController = CreateProfileViewController()
        }
    }
    
    
    func closeOnBoarding(_ viewController: UIViewController) {
        print("Closing onboarding")
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didCompleteOnboardingNotification(_:)),
                                               name: NSNotification.Name(rawValue: Notifications.OnboardingCompletedNotification),
                                               object: nil)
    }
    
    @objc fileprivate func didCompleteOnboardingNotification(_ notification: NSNotification) {
        print("Onboarding completed notification received")
        window?.rootViewController = CreateProfileViewController()
    }
    
    fileprivate func unsubscribeFromNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        unsubscribeFromNotifications()
    }
}

