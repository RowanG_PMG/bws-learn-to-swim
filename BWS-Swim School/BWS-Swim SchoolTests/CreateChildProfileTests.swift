//
//  CreateChildProfileTests.swift
//  BWS-Swim SchoolTests
//
//  Created by Rowan Louis Gontier on 22/11/19.
//  Copyright © 2019 Rowan Louis Gontier. All rights reserved.
//

import XCTest


class CreateChildProfileTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testChildNameViewModel() {
        let childVM = ChildNameViewModel()
        childVM.data.accept("Fred")
        
        assert(childVM.validateCredentials())
        
        childVM.data.accept("F")
        
        assert(!childVM.validateCredentials())
        
        childVM.data.accept("FR")
        
        assert(childVM.validateCredentials())
    }
    
    func testSwimExperienceViewModel() {
        let swimExperienceVM = SwimExperienceViewModel()
        swimExperienceVM.data.accept("New to the pool")
        
        assert(swimExperienceVM.validateCredentials())
        
        swimExperienceVM.data.accept("")
        
        assert(!swimExperienceVM.validateCredentials())
        
        swimExperienceVM.data.accept(nil)
        
        assert(!swimExperienceVM.validateCredentials())
    }
    
    func testDateOfBirthViewModel() {
          let dobVM = DateOfBirthViewModel()
          dobVM.data.accept("3 Jan 2019")
          
          assert(dobVM.validateCredentials())
          
          dobVM.data.accept("")
          
          assert(!dobVM.validateCredentials())
          
          dobVM.data.accept(nil)
          
          assert(!dobVM.validateCredentials())
      }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
